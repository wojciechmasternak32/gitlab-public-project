import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import org.testng.annotations.BeforeTest;

public class RestfulBookerTest {

    private String authToken;
    private int bookingId;

    @BeforeTest
    public void authenticate() {
        RestAssured.baseURI = "https://restful-booker.herokuapp.com";

        RequestSpecification request = given()
                .header("Content-Type", "application/json")
                .body("{\"username\": \"admin\", \"password\": \"password123\"}");

        Response response = request.post("/auth");

        response.then().statusCode(200);

        authToken = response.jsonPath().getString("token");
    }

    // Test 1: Create a new booking
    @Test
    public void testCreateBooking() {
        RestAssured.baseURI = "https://restful-booker.herokuapp.com";

        RequestSpecification request = given()
                .header("Content-Type", "application/json")
                .body("{\"firstname\": \"YourFirstName\", \"lastname\": \"YourLastName\", \"totalprice\": 100, \"depositpaid\": true, \"bookingdates\": { \"checkin\": \"2023-04-05\", \"checkout\": \"2023-04-10\" }, \"additionalneeds\": \"Breakfast\"}");

        System.out.println("Request for testCreateBooking:");
        System.out.println(request.log().all().toString());

        Response response = request.post("/booking");

        response.then().statusCode(200);

        System.out.println("Response for testCreateBooking:");
        System.out.println(response.prettyPrint());

        bookingId = response.jsonPath().getInt("bookingid");
    }

    // Test 2: Retrieve an existing booking
    @Test(dependsOnMethods = {"testCreateBooking"})
    public void testGetBooking() {
        RestAssured.baseURI = "https://restful-booker.herokuapp.com";

        Response response = given().log().all().get("/booking/" + bookingId);

        response.then().statusCode(200);
        assertThat(response.jsonPath().getString("firstname"), equalTo("YourFirstName"));
        assertThat(response.jsonPath().getString("lastname"), equalTo("YourLastName"));

        System.out.println("Response for testGetBooking:");
        System.out.println(response.prettyPrint());
    }

    // Test 3: Update an existing booking
    @Test(dependsOnMethods = {"testGetBooking"})
    public void testUpdateBooking() {
        RestAssured.baseURI = "https://restful-booker.herokuapp.com";

        RequestSpecification request = given()
                .header("Content-Type", "application/json")
                .header("Cookie", "token=" + authToken)
                .body("{\"firstname\": \"UpdatedFirstName\", \"lastname\": \"UpdatedLastName\", \"totalprice\": 120, \"depositpaid\": false, \"bookingdates\": { \"checkin\": \"2023-04-06\", \"checkout\": \"2023-04-11\" }, \"additionalneeds\": \"Lunch\"}");

        System.out.println("Request for testUpdateBooking:");
        System.out.println(request.log().all().toString());

        Response response = request.put("/booking/" + bookingId);

        response.then().statusCode(200);
        assertThat(response.jsonPath().getString("firstname"), equalTo("UpdatedFirstName"));
        assertThat(response.jsonPath().getString("lastname"), equalTo("UpdatedLastName"));

        System.out.println("Response for testUpdateBooking:");
        System.out.println(response.prettyPrint());
    }

    // Test 4: Delete an existing booking
    @Test(dependsOnMethods = {"testUpdateBooking"})
    public void testDeleteBooking() {
        RestAssured.baseURI = "https://restful-booker.herokuapp.com";

        // Deleting the booking
        Response response = given()
                .header("Cookie", "token=" + authToken)
                .log().all()
                .delete("/booking/" + bookingId);

        response.then().statusCode(201);

        System.out.println("Response for testDeleteBooking:");
        System.out.println(response.prettyPrint());

        // Verify the booking was deleted
        response = given().log().all().get("/booking/" + bookingId);
        response.then().statusCode(404);

        System.out.println("Booking successfully deleted.");
    }
}